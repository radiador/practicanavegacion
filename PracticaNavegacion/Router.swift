//
//  Router.swift
//  PracticaNavegacion
//
//  Created by formador on 30/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation
import UIKit

enum FinalRoute {
    case  darkBlue, lightBlue, third
}

class Router {
    
    static let shared = Router()
    
    private init(){}
    
    func route(to route: FinalRoute, from fromViewController: UIViewController) {
        
        switch route {
        case .darkBlue:
            routeToDark(from: fromViewController)
        case .lightBlue:
            routeToLight(from: fromViewController)
        case .third:
            routeToThird(from: fromViewController)
        }
    }
    
    private func routeToDark(from: UIViewController) {
    
        let darkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "darkViewControllerIdentifier")
        
        let navigationcontroller = from.navigationController

        navigationcontroller?.pushViewController(darkViewController, animated: true)
    }
    
    private func routeToLight(from: UIViewController) {
    
        let lightViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "lightCyanIdentifier")
        
        let navigationcontroller = from.navigationController

        navigationcontroller?.pushViewController(lightViewController, animated: true)
    }
    
    private func routeToPink(from: UIViewController) {
        
        let pinkViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pinkViewControllerIdentifier") as? PinkViewController
        
        let model = Model()
        
        pinkViewController?.model = model
        
        let navigationcontroller = from.navigationController
        
        if let pinkViewController = pinkViewController {
            navigationcontroller?.pushViewController(pinkViewController, animated: true)
        }
    }
    
    private func routeToThird(from: UIViewController) {
        
        let thirdViewController = UIViewController(nibName: "ThirdViewController", bundle: Bundle.main)
        
        let navigationcontroller = from.navigationController

        navigationcontroller?.pushViewController(thirdViewController, animated: true)
    }
}
