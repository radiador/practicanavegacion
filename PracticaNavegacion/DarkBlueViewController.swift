//
//  DarkBlueViewController.swift
//  PracticaNavegacion
//
//  Created by formador on 30/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class DarkBlueViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func organizeButtonAction(_ sender: Any) {
        
        //Instancio un ViewController, en este caso el tabBarController creado en el storyboard "tabBarControllerIdentifier"
        let tabbarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarControllerIdentifier")
        
        //Lo presento modal
        present(tabbarController, animated: true, completion: nil)
        
    }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        
        //Lanza un segue que navega hacia el tabbar
        performSegue(withIdentifier: "tabBarSegueIdentifier", sender: self)
    }
}
