//
//  ViewController.swift
//  PracticaNavegacion
//
//  Created by formador on 30/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func navegaADarkButtonAction(_ sender: Any) {
                
        Router.shared.route(to: .darkBlue, from: self)
    }
    
    @IBAction func navegaaLightButtonAction(_ sender: Any) {
        
        Router.shared.route(to: .lightBlue, from: self)
    }
    
    @IBAction func navegaAThirdButtonAction(_ sender: Any) {
        
        Router.shared.route(to: .third, from: self)
    }
    
}

