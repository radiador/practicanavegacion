//
//  LightCyanViewController.swift
//  PracticaNavegacion
//
//  Created by formador on 30/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class LightCyanViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func closeButtonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pinkSegueIdentifier", let  pinkViewController = segue.destination as? PinkViewController {
            
            pinkViewController.model = Model()
        }
    }
    
}
